import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './Footer.css';
import { Row, Col} from 'antd';
class Footer extends React.Component {
render() {
return (
/* start section Footer */
<div className="footer">
    <div className="container">
        <div className="footer-infro">
            <Col span={18} offset={3}>
            <Col md={3}>
            <div className="footer-logo">
                <img src={require('../../../img/footer-logo.svg')}/>
            </div>
            </Col>
            <Col md={7}>
            <div className="footer-email">
                <h3> Email: Cleversell@info.io</h3>
            </div>
            </Col>
            <Col md={7}>
            <div className="footer-email">
                <h3> Fax:  +02 333 867 2345</h3>
            </div>
            </Col>
            <Col md={7}>
            <div className="footer-email">
                <h3>Telephone: +02 333 769 2347</h3>
            </div>
            </Col>
            <div className="footer-copyright">
                <p> Made with  💙  by qpix.io</p>
                <p>Copyright @ qpix 2019</p>
            </div>
            </Col>
        </div>
    </div>
</div>
/* End Section Footer */
);
}
}
export default Footer;