import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './Home.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { Row, Col} from 'antd';
import Header from "./Header/Header";
import GetStarted from "./GetStarted/GetStarted";
import RetailInterface from "./RetailInterface/RetailInterface";
import IntegrateHardwares from "./IntegrateHardwares/IntegrateHardwares";
import BookInterface from "./BookInterface/BookInterface";
import IntegrateSlider from "./IntegrateSlider/IntegrateSlider";
import Footer from "./Footer/Footer";
import HrLine from "./HrLine/HrLine";
import './ResponsiveHome.css';

class Home  extends React.Component {
render() {  
return (
<div className="home-bg">
    <Header/>
    <GetStarted/>
    <BookInterface/>
    <RetailInterface/>
    <HrLine/>
    <IntegrateHardwares/>
    <HrLine/>
    <IntegrateSlider/>
    <Footer/>
</div>
);
}
}
export default Home;