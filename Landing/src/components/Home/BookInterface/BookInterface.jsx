import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './BookInterface.css';
import { Row, Col} from 'antd';
class BookInterface extends React.Component {
render() {
return (
/* start section Book Interface */
<div className="booking-interface">
    <div className="container">
         <Col xs={24} sm={24} md={24} lg={16} xl={16}>
        <img src={require('../../../img/bookings.png')}/>
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>

        <h1> Bookings  Cleversell  <br/> interface</h1>
        <p> when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining.</p>
        <div className="booking-interface__button">
            <Button type="primary">Join</Button>
        </div>
        </Col>
    </div>
</div>
/* End Section Book Interface */
);
}
}
export default BookInterface;