import React from "react";
import { Menu, Icon ,Grid , Select, Button } from 'antd';
import './GetStarted.css';
import { Row, Col} from 'antd';
const { SubMenu } = Menu;
const { Option } = Select;
function handleChange(value) {
console.log(`selected ${value}`);
}
class GetStarted extends React.Component {
render() {
return (
/* Start Section Get Started */
<div className="getstarted">
    <div className="container">

         <Col xs={{ span: 24 }} lg={{ span: 18, offset: 3 }}>

        <h1> Get started Now</h1>
        <p> Lorem Ipsum is simply dummy text of
            the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since 
            the 1500s, when an unknown printer 
            took a galley of type and scrambled it to make a type
            specimen book. It has survived not only five
            centuries, but also the leap into 
            electronic  and more recently with desktop  including versions of Lorem Ipsum.
        </p>
        <div className="getstarted-join">
            <Select defaultValue="Choose your industry" onChange={handleChange}>
                <Option value="jack">Jack</Option>
                <Option value="lucy">Lucy</Option>
            </Select>
            <Button type="primary">Join</Button>
        </div>
        {/* end get started section*/}
        </Col>
    </div>
</div>
/* End Section Get Started */
);
}
}
export default GetStarted;